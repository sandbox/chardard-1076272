<?php
// $Id$

/**
 * @file
 * The module displays a prayer map.
 */

/**
 * Implementation of hook_help(). Provides Help for the module through drupals help screen.
 */
function prayermap_help($path, $arg) {
  $output = '';
  switch ($path) {
    case 'admin/modules#name':
      $output .= t('Prayer Map');
      break;
    case 'admin/help#prayermap':
      $output .= '<p>'.
        t('This Module provides a graphical interface for adding prayers to a map. It also provides a graphical map which allows prayers to be outputted and viewed by logged in users, why? because its easier to pray for people if you know where they are and why they need prayer.') .
        '</p><h3>'.
        t('Usage:') .
        '</h3><p>'.
        t('The Prayers can be added to the map <a href="@prayermap">here</a> by following the simple instructions on the page, by any logged in users. The output of the users prayers on the map can be viewed <a href="@prayermapview">here</a>. The prayers can be viewed by clicking on the markers on the map, or clicking on the links in sidebar to the right of the map. To view the generated XML that is then read in by the Google map to display all the prayers <a href="@prayermapxml">click here</a>, then view the page source to view the generated xml.', array('@prayermap' => url('prayermap'), '@prayermapview' => url('prayermapview'), '@prayermapxml' => url('prayermapxml.xml'))) .
        '</p><p>'.
        t('The Prayers can also be viewed <a href="@prayermapplain">here</a> This page displays the same content that can be seen on the Google map but without the Google map which some users may find easier to use. It also provides the details to the administrator that are required for the admin page <a href="@prayermapadmin">here</a>. This page allows you to delete prayers that may be inappropriate for the site without having to access the database.' , array('@prayermapplain' => url('prayers'), '@prayermapadmin' => url('admin/settings/prayermap'))) .
        '</p>';
      break;
  }
  return $output;
}


/**
 * Implementation of hook_menu().
 */
function prayermap_menu() {
  $items = array();

//Set Up the menu/page callback for Creating The Prayers
  $items['node/add/prayermap'] = array(
  'title' => 'Create Prayer',
  'description' => 'Add Your Prayers To The Prayer Map so that they can be viewed by other users. Each prayer that is created will be linked back to your user account.',
  'page callback' => 'prayermap_page',
  'access arguments' => array('create prayer'),//set access permissions to create prayer
  'type' => MENU_NORMAL_ITEM,
  );

//Set Up the menu/page callback for outputting the generated XML file
  $items['prayermapxml.xml'] = array(
  'page callback' => 'prayermapxml_page',
  'access arguments' => array('view prayer map'),//set access permissions to view prayer
  'type' => MENU_CALLBACK,
  );

//Set Up the menu/page callback for Viewing The Prayers on a Google Map
  $items['prayermapview'] = array(
  'title' => 'View Prayer Map',
  'description' => 'This page allows you to view other users payers by browsing the Google map. You can also select prayers using the links on the right of the map.',
  'page callback' => 'prayermapview_page',
  'access arguments' => array('view prayer map'),//set access permissions to view prayer map
  'type' => MENU_NORMAL_ITEM,
  );
//Set Up the menu/page callback for Viewing The Prayers without a google map
  $items['prayers'] = array(
  'title' => 'View Prayers',
  'description' => 'This page shows users prayers without the Google map.',
  'page callback' => 'prayers_page',
  'access arguments' => array('view prayer'),//set access permissions to view prayer
  'type' => MENU_NORMAL_ITEM,
  );
//Set Up the menu/page callback for editing the prayers
  $items['editprayer'] = array(
  'title' => 'Edit Prayer',
  'page callback' => 'prayers_edit',
  'access arguments' => array('edit own prayer'),//set access permissions to edit own prayer
  'type' => MENU_CALLBACK, 
  );
//Set Up the menu/page callback for the admin part
  $items['deleteprayer'] = array(
  'title' => 'Delete Prayer',
  'page callback' => 'prayermapadmin_page',
  'access arguments' => array('delete own prayer'),//set access permissions to delet own prayer
  'type' => MENU_CALLBACK, 
  );

  return $items;
}

/**
 * Implementation of hook_perm(). this is the options for access to pages in drupal and will addd an option in /admin/user/permissions
 */
function prayermap_perm() {
  return array(
    'create prayer',
    'view prayer map',  
    'view prayer',
    'edit own prayer',
    'delete any prayer',
    'edit any prayer',
    'delete own prayer',
  );
  }

/**
 * Function to redirect the users when a form submission is cancelled().
 */
  function prayermap_form_cancel() {
  drupal_goto('prayermapview');
  }

/**
 * Page callback that allows the user to add a prayer to the prayer map.
 */
  function prayermap_page() {
  
  global $user;
  
  //Adds the required files to the page and sets the page header
  drupal_set_html_head('<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>');  
  drupal_add_js(drupal_get_path('module', 'prayermap') . '/javascript/MapIn.js');
  drupal_add_css(drupal_get_path('module', 'prayermap') .'/css/map.css');

  //Outputs the map, text and the Form which is created further down the page
  $map = '<p>Please add your prayer to the map by following the simple steps below, or view other users prayer <a href="/prayermapview">here</a>.</p>';
  $map .= '<p>1. Drag and Drop the marker to your desired location on the map.</p>';
  $map .= '<div id="mapouter">';
  $map .= '<div id="MapIn"></div>';
  $map .= '  <div id="infoPanel"><b>Marker status:</b><div id="markerStatus"><i>Click and drag the marker.</i></div><b>Current position:</b>
    <div id="info"></div><b>Closest matching address:</b><div id="address"></div></div><br/>'; 
  $map .= '</div>';
  $map .= drupal_get_form('map_form');

  return $map;
}

/**
 * Form callback for the prayermap_page. creates the form.
 */
function map_form($form_state) {

$form = array();

  //gets the users details so that the username can be a default vaule in a field
  global $user;

  $form['latd'] = array(
  '#type' => 'textfield',
  '#title' => t('This is the Latitude of your chosen position'),
  '#disabled' => TRUE,
  );
  $form['lngd'] = array(
  '#type' => 'textfield',
  '#title' => t('This is the longitude of your chosen position'),
  '#disabled' => TRUE,
  );
  $form['address'] = array(
  '#type' => 'hidden',
  );
  $form['lat'] = array(
  '#type' => 'hidden',
  );
  $form['lng'] = array(
  '#type' => 'hidden',
  );
  $form['name'] = array(
  '#type' => 'textfield',
  '#default_value' => $user->name,
  '#title' => t('2. Please Enter Your Name'),
  '#required' => TRUE,
  );
  $form['marker'] = array(
  '#type' => 'select',
  '#title' => t('3. Please Choose The Marker Colour'),
  '#options' => array('blue' => t('Blue'), 'green' => t('Green'), 'aqua' => t('Aqua'), 'red' => t('Red'), 'orange' => t('Orange'), 'pink' => t('Pink'), 'purple' => t('Purple'), 'yellow' => t('Yellow')), 
  );
  $form['prayer'] = array(
  '#type' => 'textarea',
  '#title' => t('4. Please Enter Your Prayer'),
  '#cols' => 60, 
  '#rows' => 5,
  '#default_value' => 'Your Prayer',
  '#required' => TRUE,
  );
  $form['submit'] = array(
  '#type' => 'submit',
  '#value' => t('Submit Your Prayer'),
  );
  $form['cancel'] = array(
  '#type' => 'button',
  '#value' => t('Cancel'),
  '#executes_submit_callback' => TRUE,
  '#submit' => array('prayermap_form_cancel'),
  );
  return ($form);
  }
  
  /**
  * Submit the form to the drupal database and output the message for a successfull submission
  */
  function map_form_submit($form_id, &$form_state) {

  //gets the users details so that the user ID can be saved to the database
  global $user;

  //Retrieves the vaues from the form
  $uid = $user->uid;
  $lat = $form_state['values']['lat'];
  $lng = $form_state['values']['lng'];
  $address = $form_state['values']['address'];
  $name = $form_state['values']['name'];
  $marker = $form_state['values']['marker'];
  $prayer = $form_state['values']['prayer'];
  $date = date('y/m/d h:i:sa');
  
  //Inserts the values into the database 
  db_query("INSERT INTO {prayermap} (uid, created, lat, lng, address, name, marker, prayer) VALUES ('%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s')",
  $uid, $date, $lat, $lng, $address, $name, $marker, $prayer);

  //Builds the successfull submission message
  $output = t('Congratulations ');
  // check_plain() makes sure there's nothing nasty going on in the text written by the user.
  $output .= check_plain($name);
  $output .= t(' you have successfully placed an ');
  $output .= check_plain($marker);
  $output .= t(' marker on the prayer map. '); 
  $form_state['redirect'] = '/prayermapview';//redirects the users when the form has been submitted
  drupal_set_message($output);
  }



  /**
 * Function to replace invalid charecters in the xml file.
 */
  function parseToXML($htmlStr) { 
  $xmlStr=str_replace('<', '&lt;', $htmlStr); 
  $xmlStr=str_replace('>', '&gt;', $xmlStr); 
  $xmlStr=str_replace('"', '&quot;', $xmlStr); 
  $xmlStr=str_replace("'", '&#39;', $xmlStr); 
  $xmlStr=str_replace("&", '&amp;', $xmlStr); 
  return $xmlStr; 
  }

  /**
  * Page callback uses a SQL query to output a xml file which is then read by the google map.
  */
  function prayermapxml_page() {
  
  header("Content-type: text/xml");

  //initial setup to limit the results to those posted in the last 3 months
  $ago = strtotime("-3 months");
  $ago_string = date('Y-m-d', $ago);
  //SQL query
  $sql = 'SELECT * FROM {prayermap} WHERE created > "%s" ORDER BY created DESC';  // selects all fields from the praqyermap table, orders results by the date and time descending
  $results = db_query($sql, $ago_string); // Run the query
  echo "<markers>"; // Wrap each record in a <markers> tag
  while ($fields = db_fetch_array($results)) { // Get the next result as an associative array
  // Iterate over all of the fields in this row
    echo '<marker ';
    echo 'uid="' . parseToXML($fields['uid']) . '" ';
    echo 'name="' . parseToXML($fields['name']) . '" ';
    echo 'prayer="' . parseToXML($fields['prayer']) . '" ';
    echo 'type="' . parseToXML($fields['marker']) . '" ';
    echo 'lat="' . $fields['lat'] . '" ';
    echo 'lng="' . $fields['lng'] . '" ';
    echo '/>';
  } 
  echo "</markers>";  
  }



  /**
  * Page callback that allows the user to view the prayers by displaying a google map which reads in the xml file.
  */
  function prayermapview_page() {

  //Adds the required files to the page and sets the page header
  drupal_set_html_head('<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>');  
  drupal_add_js(drupal_get_path('module', 'prayermap') . '/javascript/MapOut.js');
  drupal_add_css(drupal_get_path('module', 'prayermap') .'/css/map.css');
  
  //Outputs the map and text
  $map = '<p>Please browse the map below to view other users prayers. You can also select prayers using the links on the right of the map. If you would like to add a prayer to the map for yourself or someone else please click <a href="/node/add/prayermap">here</a>.
  If you would like to view the prayers without the Google Map or you would like to edit or delete your prayers then please click <a href="prayers">here</a>.';
  $map .= '<div id="mapouter">';
  $map .= '<div id="MapOut"></div>';
  $map .= '<div id="side_bar"></div>';
  $map .= '</div>';
  return $map;
  }


  /**
   * Page callback that allows the user to view the prayers by displaying a page with all the outputted prayers.
   */
  function prayers_page() { 
  //gets the users details so that the user ID can be used in the IF statments
  global $user;

  //gets the modules css file
  drupal_add_css(drupal_get_path('module', 'prayermap') .'/css/map.css');

 //initial setup to limit the results to those posted in the last 3 months
  $ago = strtotime("-3 months");
  $ago_string = date('Y-m-d', $ago);  
  //SQL query
  $sql = 'SELECT * FROM {prayermap} WHERE created > "%s" ORDER BY created DESC';  // selects all fields from the praqyermap table, orders results by the date and time descending
  $results = db_query($sql, $ago_string); // Run the query
  $output = "<p> This page displays all the prayers that can be seen on the google map, in a list that can be browsed by scrolling down through the page.</p>";
  $output .=  '<div id="prayers">'; // Wrap each record in a <div> tag
  while ($fields = db_fetch_array($results)) { // Get the next result as an associative array

 // Get the date because it is currently in the yyyy-mm-dd format
  $date = ($fields['created']);
  
 // Iterate over all of the fields in this row
    $output .= '<div class="containerOuter">';
    $output .= '<div class="container">';
    $output .= '<div class="maptitle">';
    $output .= 'Prayer ID: ' . ($fields['id']) . ', ' .'<a href="user/' . ($fields['uid']) . '">' . 'User Name: ' . ($fields['name']) . '</a>';
    $output .= '<div class="postDate">';
    $output .= 'Posted: ' . date('l d F Y,', strtotime($date)) . ' At: ' . date('h:i:s', strtotime($date));// change the formatt of the date
    $output .= '</div></div>';
    $output .= '<div class="location">' . 'Location: ' . ($fields['address']) . '.' . '</div>' ;
    $output .= '<div class="prayer">';  
    $output .= '<p>' . ($fields['prayer']) . '</p></div><div class="buttons">';
    
    /*
    * if statment which if the users id and the user id from the database is the same and the user type has permissions to edit own prayer will display the edit button
    * or the user type has permissions to edit any prayer
    * or the user type has administrative permissions
    */
    if (user_access('edit own prayer') and $uid = $user->uid == $fields['uid'] or user_access('administer') or user_access('edit any prayer')) {
    //form which passes the prayer id to the edit prayer page on submit of the button
    $output .= '<div class="edit"> 
		  <form action="editprayer" method="post">
		  <input type="hidden" name="variable" value="' . ($fields['id']) . '"/>
		  <input type="submit" value="Edit" />
		  </form></div>';
    }
    /*
    * if statment which if the users id and the user id from the database is the same and the user type has permissions to delete own prayer will display the edit button
    * or the user type has permissions to delete any prayer
    * or the user type has administrative permissions
    */
    if ( user_access('delete own prayer') and $uid = $user->uid == $fields['uid'] or user_access('administer') or user_access('delete any prayer')) {
    //form which passes the prayer id to the delete prayer page on submit of the button
    $output .= '<div class="delete">
		  <form action="deleteprayer" method="post">
		  <input type="hidden" name="variable" value="' . ($fields['id']) . '"/>
		  <input type="submit" value="Delete" />
		  </form></div>';
    }    
    $output .= '</div></div></div>';
  } 
  $output .= "</div>";  
  return $output;
  }

  /**
   * Page callback that allows the user to edit a prayer that they have posted.
   */
  function prayers_edit() {
  
  //Adds the required files to the page and sets the page header
  drupal_set_html_head('<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>');  
  drupal_add_js(drupal_get_path('module', 'prayermap') . '/javascript/MapIn.js');
  drupal_add_css(drupal_get_path('module', 'prayermap') .'/css/map.css');

  //Outputs the map, text and the Form which is created further down the page

  $map .= '<p>Please Edit your prayer by following the simple steps below.</p>';
  $map .= '<p>1. Re-select your position on the map by Draging and Droping the marker on to your desired location on the map.</p>';
  $map .= '<div id="mapouter">';
  $map .= '<div id="MapIn"></div>';
  $map .= '  <div id="infoPanel"><b>Marker status:</b><div id="markerStatus"><i>Click and drag the marker.</i></div><b>Current position:</b>
    <div id="info"></div><b>Closest matching address:</b><div id="address"></div></div><br/>'; 
  $map .= '</div>';
  $map .= drupal_get_form('prayermap_edit_form');

  return $map;
}

/**
 * Form callback for the prayers_edit. creates the form.
 */
function prayermap_edit_form($form_state) {
  
  $pid = $_POST["variable"];
  
  //SQL query to populate the fields of the form whith the data that has been previously submitted
  $sql = "SELECT * FROM {prayermap} WHERE id = %d";//Selects all fields from the table prayermap where the id = the id passed by the edit button
  $results = db_query($sql, $pid);
  while ($fields = db_fetch_array($results)) {// Get the next result as an associative array
  // Iterate over all of the fields in this row
  $prayer = ($fields['prayer']);
  $marker = ($fields['marker']);
  $name = ($fields['name']);
  $lat = ($fields['lat']);
  $lng = ($fields['lng']);
  }
  
$form = array();

  $form['latd'] = array(
    '#type' => 'textfield',
    '#title' => t('This is the Latitude of your chosen position'),
    '#disabled' => TRUE,
    );
  $form['lngd'] = array(
    '#type' => 'textfield',
    '#title' => t('This is the longitude of your chosen position'),
    '#disabled' => TRUE,
    );
  $form['address'] = array(
    '#type' => 'hidden',
    );
  $form['pid'] = array(
    '#default_value' => $pid,
    '#type' => 'hidden',
    );
  $form['lat'] = array(
    '#type' => 'hidden',
    );
  $form['lng'] = array(
    '#type' => 'hidden',
    );
  $form['name'] = array(
    '#type' => 'textfield',
    '#default_value' => $name,
    '#title' => t('2. Please Enter Your Name'),
    '#required' => TRUE,
    );
  $form['marker'] = array(
    '#type' => 'select',
    '#title' => t('3. Please Choose The Marker Colour'),
    '#default_value' => $marker,
    '#options' => array('blue' => t('Blue'), 'green' => t('Green'), 'aqua' => t('Aqua'), 'red' => t('Red'), 'orange' => t('Orange'), 'pink' => t('Pink'), 'purple' => t('Purple'), 'yellow' => t('Yellow')),
    );
  $form['prayer'] = array(
    '#type' => 'textarea',
    '#default_value' => $prayer,
    '#title' => t('4. Please Enter Your Prayer'),
    '#cols' => 60, 
    '#rows' => 5,
    '#required' => TRUE,
    );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit Your Prayer'),
    );
  $form['cancel'] = array(
  '#type' => 'button',
  '#value' => t('Cancel'),
  '#executes_submit_callback' => TRUE,
  '#submit' => array('prayermap_form_cancel'),
  );
  return ($form);
}

/**
 * Submit the form to the drupal database and output the message for a successfull submission
 */
function prayermap_edit_form_submit($form_id, &$form_state) {

//gets the users details so that the user ID can be saved to the database
  global $user;

//Retrieves the vaues from the form
  $uid = $user->uid;
  $lat = $form_state['values']['lat'];
  $lng = $form_state['values']['lng'];
  $address = $form_state['values']['address'];
  $name = $form_state['values']['name'];
  $marker = $form_state['values']['marker'];
  $prayer = $form_state['values']['prayer'];
  $pid = $form_state['values']['pid']; // passes the prayer ID from the hidden row in the form
  $date = date('y/m/d h:i:sa');
  
//Updates the values in the database
  db_query("UPDATE {prayermap} SET uid = '%s', created = '%s', lat = '%s', lng = '%s', address = '%s', name = '%s', marker = '%s', prayer = '%s' WHERE id = %d", // where the prayer ID = prayer ID that was passed from the hidden row in the form
  $uid, $date, $lat, $lng, $address, $name, $marker, $prayer, $pid);
  
  
//Builds the successfull submission message
  $output = t('Congratulations ');
  // check_plain() makes sure there's nothing nasty going on in the text written by the user.
  $output .= check_plain($name);
  $output .= t(' you have successfully updated your prayer. '); 
  $form_state['redirect'] = '/prayermapview';
  drupal_set_message($output);
}



/**
 * Page callback that allows the administrator to delet prayers by their ID
 */
function prayermapadmin_page() {
  
  $pid = $_POST["variable"];
  //SQL query to retrieve the prayer that has been previously submitted so it can be outputted
  $sql = "SELECT * FROM {prayermap} WHERE id = %d";//Selects all fields from the table prayermap where the id = the id passed by the edit button
  $results = db_query($sql, $pid);
  while ($fields = db_fetch_array($results)) {// Get the next result as an associative array
  // Iterate over all of the fields in this row
  $prayer = ($fields['prayer']);
  }
  
  //Outputs the prayer, text and the Form which is created further down the page
  $output = "<p>This form allows you to delete your prayer. The ID of the prayer you are going to delet is shown below. PLEASE THINK CAREFULLY before deleting any prayers from the website.</p>";
  $output .= '<h3>Prayer:</h3>';
  $output .= '<div><p>' . ($prayer) . '</p></div>';
  $output .= drupal_get_form('prayermap_admin_form');
  return $output;
}

/**
 * Form callback for the prayermapadmin_page.
 */
function prayermap_admin_form($form_state) {
$form = array();

  $form['prayeriddisabled'] = array(
  '#type' => 'textfield',
  '#default_value' => $_POST["variable"],
    '#title' => t('ID of the Prayer'),
    '#disabled' => TRUE,
  );
  $form['prayerid'] = array(
    '#default_value' => $_POST["variable"],
    '#type' => 'hidden',
    );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Delete'),
  );
  $form['cancel'] = array(
  '#type' => 'button',
  '#value' => t('Cancel'),
  '#executes_submit_callback' => TRUE,
  '#submit' => array('prayermap_form_cancel'),
  );
  return ($form);
  }
//Validation callback for the admin form
  function prayermap_admin_form_validate($form, &$form_state) {
    if (!is_numeric($form_state['values']['prayerid'])) {
      form_set_error('prayerid', t('The value entered must be numeric'));
    }
  }
/**
 * Submit the form to the drupal database and output the message for a successfull submission
 */
  function prayermap_admin_form_submit($form_id, &$form_state) {
  
  $pid = $form_state['values']['prayerid'];
    // SQL Delete Query
  db_query("DELETE FROM {prayermap} WHERE id = %d", $pid);//where the id = the prayer id that was passed by the form
    
  $output = t('Congratulations ');
  $output .= t(' you have successfully Deleted a prayer that had the ID: ');
    // check_plain() makes sure there's nothing nasty going on in the text written by the user.
  $output .= check_plain($pid);
  $form_state['redirect'] = '/prayers';
  drupal_set_message($output);
}
