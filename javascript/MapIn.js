$(document).ready(function() {
 var geocoder = new google.maps.Geocoder();

function geocodePosition(pos) {
  geocoder.geocode({
    latLng: pos
  }, function(responses) {
    if (responses && responses.length > 0) {
      updateMarkerAddress(responses[0].formatted_address);
    } else {
      updateMarkerAddress('Cannot determine address at this location.');
    }
  });
}

function updateMarkerStatus(str) {
  document.getElementById('markerStatus').innerHTML = str;
}

function updateMarkerPosition(latLng) {
  document.getElementById('info').innerHTML = [
    latLng.lat(),
    latLng.lng()
  ].join(', ');
   document.getElementById("edit-lat").value=latLng.lat();
   document.getElementById("edit-lng").value=latLng.lng();
   document.getElementById("edit-latd").value=latLng.lat();
   document.getElementById("edit-lngd").value=latLng.lng();
}

function updateMarkerAddress(str) {
  document.getElementById('address').innerHTML = str;
  document.getElementById("edit-address").value = str;
}

  var latLng = new google.maps.LatLng(50.87123673581049, -2.963219092654981);
  var map = new google.maps.Map(document.getElementById('MapIn'), {
    zoom: 13,
    center: latLng,
    mapTypeId: google.maps.MapTypeId.ROADMAP,
	scaleControl: true
  });
  var marker = new google.maps.Marker({
    position: latLng,
    title: 'Prayer Point',
    map: map,
    draggable: true
  });
  
  // Update current position info.
  updateMarkerPosition(latLng);
  geocodePosition(latLng);
  
  // Add dragging event listeners.
  google.maps.event.addListener(marker, 'dragstart', function() {
    updateMarkerAddress('Dragging...');
  });
  
  google.maps.event.addListener(marker, 'drag', function() {
    updateMarkerStatus('Dragging...');
    updateMarkerPosition(marker.getPosition());
  });
  
  google.maps.event.addListener(marker, 'dragend', function() {
    updateMarkerStatus('Drag ended');
    geocodePosition(marker.getPosition());
  });
    });