 //<![CDATA[
      // this variable will collect the html which will eventually be placed in the side_bar 
      var side_bar_html = ""; 
    
      // arrays to hold copies of the markers and html used by the side_bar 
      // because the function closure trick doesnt work there 
      var gmarkers = []; 

     // global "map" variable
      var map = null;

    var customIcons = {
      blue: {
        icon: 'http://www.google.com/intl/en_us/mapfiles/ms/micons/blue-dot.png'
      },
      aqua: {
        icon: 'http://www.google.com/intl/en_us/mapfiles/ms/micons/ltblue-dot.png'
      },
      green: {
        icon: 'http://www.google.com/intl/en_us/mapfiles/ms/micons/green-dot.png'
      },
      orange: {
        icon: 'http://www.google.com/intl/en_us/mapfiles/ms/micons/orange-dot.png'
      },
      pink: {
        icon: 'http://www.google.com/intl/en_us/mapfiles/ms/micons/pink-dot.png'
      },
      red: {
        icon: 'http://www.google.com/intl/en_us/mapfiles/ms/micons/red-dot.png'
      },
      purple: {
        icon: 'http://www.google.com/intl/en_us/mapfiles/ms/micons/purple-dot.png'
      },
      yellow: {
        icon: 'http://www.google.com/intl/en_us/mapfiles/ms/micons/yellow-dot.png' 
      }

    };
    
    var iconShadow = new google.maps.MarkerImage('http://maps.google.com/mapfiles/ms/micons/msmarker.shadow.png',

      // The shadow image is larger in the horizontal dimension

      // while the position and offset are the same as for the main image.

      new google.maps.Size(59, 32),

      new google.maps.Point(0,0),

      new google.maps.Point(16, 32));
    
    
 $(document).ready(function() {   
    
     var myOptions = {
     		zoom: 13,
      		center: new google.maps.LatLng(50.87347,-2.960815),
      		mapTypeControl: true,
    		mapTypeControlOptions: {style: google.maps.MapTypeControlStyle.HORIZONTAL_BAR},
      		navigationControl: true,
      		navigationControlOptions: {style: google.maps.NavigationControlStyle.DEFAULT},
      		scaleControl: true,    mapTypeId: google.maps.MapTypeId.ROADMAP

  			}
    
    
      var map = new google.maps.Map(document.getElementById("MapOut"),myOptions);
      var infoWindow = new google.maps.InfoWindow;

      // Change this depending on the name of your PHP file
      downloadUrl("/prayermapxml.xml", function(data) {
        var xml = data.responseXML;
        var markers = xml.documentElement.getElementsByTagName("marker");
        for (var i = 0; i < markers.length; i++) {
          var uid = markers[i].getAttribute("uid");
	  var name = markers[i].getAttribute("name");
          var prayer = markers[i].getAttribute("prayer");
          
          var type = markers[i].getAttribute("type");          
          var point = new google.maps.LatLng(
              parseFloat(markers[i].getAttribute("lat")),
              parseFloat(markers[i].getAttribute("lng")));
          var html ="<div class='mapcont'>"+"<span class='html'>" + "<span class='title'>" + "<a href='user/" + uid + "'>" + name + ":</a>" + "</span>" + "<br/>" + prayer + "</span>"+"</div>";
          var icon = customIcons[type] || {};
          var marker = new google.maps.Marker({
            map: map,
            position: point,
            icon: icon.icon,
            shadow: iconShadow

          });
          bindInfoWindow(marker, map, infoWindow, name, html);
          }
        // put the assembled side_bar_html contents into the side_bar div
        document.getElementById("side_bar").innerHTML = side_bar_html;
      });
          });
    
      function bindInfoWindow(marker, map, infoWindow, name, html) {
      google.maps.event.addListener(marker, 'click', function() {
        infoWindow.setContent(html);
        infoWindow.open(map, marker);

      });
      // save the info we need to use later for the side_bar
    gmarkers.push(marker);
    // add a line to the side_bar html
    side_bar_html += '<a href="javascript:myclick(' + (gmarkers.length-1) + ')">'+"- " + name + '<\/a><br/>';
    }
    
    // This function picks up the click and opens the corresponding info window
function myclick(i) {
  google.maps.event.trigger(gmarkers[i], "click");
}

    function downloadUrl(url, callback) {
      var request = window.ActiveXObject ?
          new ActiveXObject('Microsoft.XMLHTTP') :
          new XMLHttpRequest;

      request.onreadystatechange = function() {
        if (request.readyState == 4) {
          request.onreadystatechange = doNothing;
          callback(request, request.status);
        }
      };

      request.open('GET', url, true);
      request.send(null);
    }

    function doNothing() {}

    //]]>
